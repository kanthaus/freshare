---
date: 2020-02-12
title: houseDataProject
tags: data, lod, network, collectives, cooperation, supercooperation, sharing,
type: slide
author: doug
---

<style>
    .reveal section figcaption {
        font-size: 18pt;
        font-style: italic;
    }
    .reveal section p,
    .reveal section li {
        font-size: 26pt;        
    }    
</style>

# Freshare
## _Fresh, shared data_
### Initial project conception
#### Doug, 2020-02-16

(View as [slideshow](https://codi.kanthaus.online/p/freshareInitial#/))

---

**TL;DR:** Infrastructure to help 1) collectives create, host and maintain structured data about themselves, and 2) everyone to access that data.

---

## Quotes for context

> _"The only thing that will redeem mankind is cooperation"_  

—Bertrand Russel, _Man's peril, 1954-55_ (reflecting on the nuclear arms race)

> _"4. computers could be the vehicle for dramatically improving this capability_ [cooperation]_."_  

—Douglas Engelbart, [concluding point of his world-improvement philosophy](https://en.wikipedia.org/wiki/Douglas_Engelbart#Guiding_philosophy)

---

### The human scale

<figure>
  <img style="width:65%;margin:auto;display:block;" src="https://kanthaus.online/user/pages/pics/kube02color.jpg" />
  <figcaption>Peope often manage to be happy in small groups</figcaption>
</figure>

Where should one begin to improve cooperation?  Perhaps the relationship between one and another, and then the aggregation of such relationships into collectives. As far as eyes can meet, humans have some innate abilities to build relationships and collectives. 

---

### Scaling issues

<figure>
  <img style="width:65%;margin:auto;display:block;" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.ventureharbour.com%2Fwp-content%2Fuploads%2F2014%2F05%2Fdunbars-number-1024x608.jpg&f=1&nofb=1" />
  <figcaption>Dunbar's research suggests social conncectivity is cognitively limited</figcaption>
</figure>

This ability deteriorates as groups get larger, more fluxional or disperse. The structures that have emerged at the 'impersonal' level—hierarchies, markets, corporations—have proven incompetent for stopping biosphere degredation, and often generate social environments which are hostile towards cooperation generally.

---

### Supercooperation

<figure>
  <img style="width:65%;margin:auto;display:block;" src="https://www.pnas.org/content/pnas/115/33/8252/F2.large.jpg?width=800&height=600&carousel=1" />
  <figcaption><a href="https://www.pnas.org/content/115/33/8252">'Trajectories of the Earth System in the Anthropocene'</a>—Hothouse earth unavoidable without supercooperation?</figcaption>
</figure>

Cooperation at a global, continental, national, regional and even city-level would require not just cooperation between individuals, but cooperation between _collectives_. This could be through higher-order collectives (i.e. collectives of collectives, 'supercollectives'), networks, or as-yet unidentified forms. Since the successful forms of organization may not yet be known, it is important to enable the creation of different forms rather than incorporating collectives into a prescriptive form.

---

### Collective as trust-generating unit

<figure>
  <img style="width:65%;margin:auto;display:block;" src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fblog.oup.com%2Fwp-content%2Fuploads%2F2013%2F03%2FiStock_000013284732XSmall.jpg&f=1&nofb=1" />
  <figcaption>Trust generated by collectives becomes a resource</figcaption>
</figure>

Cooperation between collectives is especially interesting, since collectives internally generate trust. That a given individual is part of a collective (and hasn't been excluded) is a form of self-referential trust. Thus if collectives X and Y trust each other, individual A who is part of collecive X can provisionally trust individual B who is part of collective Y. Cooperation between existing cooperators thus appears to be 'low-hanging fruit'.

---

### Existence discovery

<figure>
  <img style="width:65%;margin:auto;display:block;" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fexperiencetalk.files.wordpress.com%2F2014%2F12%2F404error-page.gif&f=1&nofb=1" />
  <figcaption>Can't do anything with entities you don't know to exist</figcaption>
</figure>

The first step to 'super-cooperation' is knowledge of existence, _discovery_. You can't work with someone you don't know to exist. For almost 30 years we've been blessed with websites and many collective have their own. Here they can put information about themselves and other people—who manage to find the site among the billions of others—can read it. However, reading all the web-pages to find the ones you're interested in is _not_ an option.

---

## Existing 'collectors'
Many initiatives have created websites which list, map or otherwise 'collect' information about initiatives (herein 'collectors'). Collectors for residential collectives include:
- [Diggers and Dreamers](https://www.diggersanddreamers.org.uk) 
- [GEN](https://ecovillage.org/)
- [totalism.org 'alike' list](https://e2h.totalism.org/e2h.php?_=alike)
- [Embassy Network](https://embassynetwork.com/)
- ... to name a few

---

## Issues with collectors
### Primary
- **Unknown data freshness**. Data on collectors arrives there intransparently, probably manually, with meta-data often unknown or missing. This makes it hard to know if the recorded data is still current.
- **Unstructured data**. The format in which data is recorded may be in formats which impede reusability. This increases the cost-of-transfer between systems, and impedes emergent supercooperation.

---

### Secondary
- **User burden**. For collectives to create their own pages on collector sites require familiarization with a new UX, and possibly new account creation. 
- **Data duplication**. If collectives hold their own data, the collector data may not match. Manual syncing is tedious, error-prone and potentially lossy.
- **Data vulnerability**. Collectives are at the mercy of the collector, which could reject or delete them.

---

<figure>
  <img style="width:100%;margin:auto;display:block;" src="https://codi.kanthaus.online/uploads/upload_de01fb20b4b619d5057edbac6fce133a.jpg" />
  <figcaption>Collector issues seen on <a href=" https://www.diggersanddreamers.org.uk/communities/existing/faslane-peace-camp">Diggers and Dreamers</a></figcaption>
</figure>

---

## Emergent cooperation
This project aims to address the mentioned issues, to enable as-yet-unknown interactions. Some of these emergent cooperations written as user stories include:
- I want to find software projects made by members of collectives
- I want to start a federated feed of all collectives that have ActivityPub
- I'm looking for the nearest collective to me
- I'm doing research on the positive impact of collectives and need data
- I want to contact collectives to develop a supercollective
- I want create a visualization of all known collectives
- I want to know the resources available for use in other collectives
- ...

---

## One possible implementation
One possible implementation has 2 major components:
1. toolage for simple creation and editing of self-hosted data
  1.1. this could include interfaces for SSGs/CMSs/command-line, etc
1. an aggregator which automatically monitors/enourages data freshness

---

<figure>
  <img style="width:100%;margin:auto;display:block;" src="https://codi.kanthaus.online/uploads/upload_aef88fb6c68261beb3e6624721984a6d.png" />
  <figcaption>A solution that would naïvely solve the 5 issues with current attempts</figcaption>
</figure>

---

## Technical inspiration
- [libreho.st](https://libreho.st/), [source](https://lab.libreho.st/librehosters/librehost-api), page that aggregates librehosters who add a .json file to their website _and_ add themselves to the 'scrape file'. (i.e. relatively hight tech barrier)
- [letsencrypt](https://letsencrypt.org/), [source](https://github.com/letsencrypt), provides TLS certificates, and automatically notifies people to update theirs if necessary

---

## Possible income streams
- ask for donations
- crowd-funding
- gov. funding
  - sounds like it most closely aligns with [SDG 11](https://ec.europa.eu/international-partnerships/sustainable-development-goals_en), "Sustainable cities and communities"

---

## KPIs
- Number of participating collectives
- Data addition to already participating collectives
