**It appears https://murmurations.network/ is doing what I was thinking of! Go there if interested in implementation.**


# Freshare

Fresh, shared data for supercooperation

## Problems

![](https://gitlab.com/kanthaus/freshare/-/raw/master/diggersAndDreamersExample.svg.png)

## A solution

![](https://gitlab.com/kanthaus/freshare/-/raw/master/basicProposalDiagram.svg.png)

## More

🎥 Initial slideshow: https://codi.kanthaus.online/p/freshareInitial#/

💬 Pad: https://codi.kanthaus.online/fresharePad?edit
